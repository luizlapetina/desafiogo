FROM golang AS builder
WORKDIR /app/
COPY desafio.go /app/
RUN go build desafio.go
RUN rm desafio.go

FROM scratch
COPY --from=builder /app /app
ENTRYPOINT [ "/app/desafio" ]